## Demo Project:
Dynamically Increment Application version in Jenkins Pipeline
## Technologie sused:
Jenkins, Docker, GitLab, Git, Java, Maven
## Project Decription:
* Configure CI step: Increment patch version
* Configure CI step: Build Java application and clean old artifacts
* Configure CI step: Build Image with dynamic Docker Image Tag and push Image to private DockerHub repository
* Configure CI step: Commit version update of Jenkins back to Git repository
* Configure Jenkins pipeline to not trigger automatically on CI build commit to avoid commit loop
## Description in details:
### Configure CI step: Increment patch version
__Step 1:__ Open your `Jenkinsfile` and add new stage `increment version` before build stages

__Note:__ You can read about it [here](https://www.mojohaus.org/build-helper-maven-plugin/parse-version-mojo.html)
```groovy
stage('increment version') {
    steps {
        script {
            echo 'incrementing app version...'
            sh 'mvn build-helper:parse-version versions:set \
                -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                versions:commit'
        }
    }
}
```
### Configure CI step: Build Java application and clean old artifacts
__Step 1:__ Change `sh` line in `build app` block  with `mvn clean package` to __avoid__ problems with existing folder 
```groovy
stage('build app') {
    steps {
        script {
            echo "building the application..."
            sh 'mvn clean package'
        }
    }
}
```
### Configure CI step: Build Image with dynamic Docker Image Tag and push Image to private DockerHub repository

__Step 1:__ In docker  stage pay attention on `docker build` and `docker push` commands - change hardcoded verion `jma-2.0` on variable `${IMAGE_NAME}`

__Note:__ We use double quotes in those lines because we have variables in code
```groovy
stage('build image') {
    steps {
        script {
            echo "building the docker image..."
            withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                sh "docker build -t nanajanashia/demo-app:${IMAGE_NAME} ."
                sh "echo $PASS | docker login -u $USER --password-stdin"
                sh "docker push nanajanashia/demo-app:${IMAGE_NAME}"
            }
        }
    }
}
```
__Step 2:__ Return to `increment version` block and add new versions variable in the end of block of code
__Note:__ We use regular expression because we don't know correct version of app
```groovy
def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
```
__Step 3:__ Because this is a matcher, this will try to match every single version tag it can find inside the pom.xml and save it into an array. This give us another array with version tag and its children elements  

```groovy
def version = matcher[0][1]
```
__Step 4:__  Assign environment in the same block
```groovy
env.IMAGE_NAME = "$version-$BUILD_NUMBER"
```
>#### Replace new version in Dockerfile
>__Step 1:__  Open `Dockefile ` and configure it for dynamic tag
>* Change in `COPY` line this part `java-maven-app-1.1.0-SNAPSHOT.jar` on this `java-maven-app-*.jar`. Because older version doesn't work anymore. It let you change version dynamically
>```yaml
>COPY ./target/java-maven-app-*.jar /usr/app/
>```
>* Change `ENTRYPOINT` string because it doesn't work this way (regular expressions) on command
>```yaml
>CMD java -jar java-maven-app-*.jar 
>```

### Configure CI step: Push Image to private DockerHub repository

### Configure CI step: Commit version update of Jenkins back to Git repository
__Step 1:__ Create new stage in `Jenkinsfile` after all stages (build app,build iamge, deploy) that called `commit version update`
```groovy
stage (`commit version update`) {
    steps {
        script {

        }
    }
}
```
__Step 2:__ Add excess credentials to git repository `gitlab-credentials`
```groovy
script {
    withcredentials ([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {

    }
}
```
__Step 3:__ Inside this block execute commands
```groovy
withcredentials ([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
    sh 'git add .'
    sh 'git commit -m "CI: version bump"'
    sh 'git push origin  HEAD:jenkins-job'
}
```
__Step 4:__ Tell Jenkins what is  origin - set origin adress
```sh
    sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/nanuchi/java-maven-app.git"
    sh 'git add .'
    sh 'git commit -m "CI: version bump"'
    sh 'git push origin  HEAD:jenkins-job'
```
__Step 5:__ Add  information about Git repository 
### Configure Jenkins pipeline to not trigger automatically on CI build commit to avoid commit loop
>#### Before to configure Jenkins pipeline
>__Step 1:__ Add this configuration inside `Jenkinsfile`before (__dont forget change e-mail and username__)
>```sh
> sh 'git config --global user.email "jenkins@example.com"'
>sh 'git config --global user.name "jenkins"'
>
>sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/nanuchi/java-maven-app.git"
>sh 'git add .'
>sh 'git commit -m "ci: version bump"'
>sh 'git push origin HEAD:jenkins-jobs'
>```
>__Note:__ git config here for __the first time__ run!

__Step 1:__  Open Jenkis UI and install plugin `ignore commiter strategy`

__Step 2:__ Back to your pipeline/multi-branch pipeline open `Configure`, inside `branch sources`
>Add `Ignore Commit strategy`
>Add e-mail or list of emails that should be ignored
>Allow builds when a chageset contaoins non-ignored autor(s)

