## Demo Project:
Install Jenkins on DigitalOcean
## Technologies used:
Jenkins, Docker, DigitalOcean, Linux
## Project Decription:
* Set up and run Jenkins as Docker container
* Initialize Jenkins
## Description in details:
### Before to strart:
__Step 1:__ Create an Ubuntu server on DigitalOcean

__Step 2:__ Create firewall for this server - open port `22` and `8080`
### Set up and run Jenkins as Docker container
__Step 1:__ Install docker
```sh
apt install docker.io
```
__Step 2:__ Run Jenkins as Docker container and assign volume for it
```sh
docker run -p 8080:8080 -p 50000:50000 -d \
-v jenkins_home:/var/jenkins_home jenkins/jenkins:lts
```
### Initialize Jenikins
__Step 1:__ connect to container using `docker exec -it "container_id" bash`

__Step 2:__ copy pwd from `/var/jenkins_home/secrets/initialAdminPassword`
```sh
cat /var/jenkins_home/secrets/initialAdminPassword
```

__Step 3:__ Paste your pwd in Jenkins, configure it and create admin user

