## Demo Project:
Configure Webhook to trigger CI Pipeline automatically on every change
## Technologies used:
Jenkins, GitLab, Git, Docker, Java, Maven
## Project Decription:
* Install GitLab Plugin in Jenkins
* Configure GitLab access token and connection to Jenkins in GitLab project settings
* Configure Jenkins to trigger the CI pipeline,whenever a change is pushed to GitLab
## Description in details:
### Install GitLab Plugin in Jenkins
__Step 1:__ Open Jenkins UI and go to `Manage Jenkins/Manage Plugins` and install `gitlab` plugin
### Configure GitLab access token and connection to Jenkins in GitLab project settings
__Step 1:__ In Jenkins UI go to `Manage Jenkins/Configure System` and configure `gitlab` plugin:
* Connection name (whatever you want)
* Gitlab host URL (e.g. https://gitlab.com/ or link on your hosted server)
* Credentials (using gitlab API token)
#### To create token 
>__Step 1:__ Open your gitlab profile and open `User/Preferences/Access tokens`
>
>__Step 2:__ Create token:
>* Name
>* Expires at
>* Permissions (choose API)
>
>__Step 3:__ Save created token

### Configure Jenkins to trigger the CI pipeline,whenever a change is pushed to GitLab
#### For single Pipeline 
__Step 1:__ Open your pipeline and configure it
> In General tab you can find `Gitlab Connection` and choose your gitlab account
>
> __Note:__ you can have many gitlab accounts
> 
>Below you can find `Build Triggers` and choose trigger behavior
>
>* Build when a change is pushed to GitLab. GitLab webhook
>>Push Events
>> Opened Merge Request Events

__Step 2:__ Open return to your project on `gitlab` and open `Settings/Integrations`, open Jenkins CI

__Step 3:__ Configure it:
>* Enable integrations
>* Trigger (push)
>* Jenkins URL
>
>__Note:__ If you running Jenkins on your local host, this is not gonna work because Gitlab integrations do not allow local host here. It has to be real IP adress or a domain name
>* Project name (job name inside Jenkins)
>* Username (of Jenkins)
>* Password (Of Jenkins)

*(Optional)* __Step 4:__ In this tab you can run `Test settings` and see build in Jenkins UI 

#### For multi-branch pipeline
__Step 1:__ Install another plugin for jenkins in `Manage Jenkins/Manage Plugins` that called `Multibranch Scan Webhook Trigger`

__Step 2:__ Go back to dashboard and inside multi-branch pipeline open `Configure`. In `Build Configuration` tab and see `Scan Multibranch Pipeline Triggers` block

__Step 3:__ Enabe `Scan by webhook` and name the token (whatever you want) and copy  Jenkins_URL

__Step 4:__ Open `Settings/Webhooks` in Gitlab and configure it and add webhook:
>URL (Paste your `Jenkins_URL` here)

__Step 5:__ Save changes in Jenkins

*(Optional)* __Step 6:__ You can test it in gitlab