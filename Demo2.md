## Demo Project:
Createa CI Pipeline with Jenkinsfile (Freestyle, Pipeline, Multibranch Pipeline)
## Technologiesused:
Jenkins, Docker, Linux, Git, Java, Maven
## Project Decription:
CI Pipeline for a Java Maven application to build and push to the repository
* Install Build Tools (Maven, Node) in Jenkins
* Make Docker available on Jenkins server
* Create Jenkins credentials for a git repository
* Create different Jenkins job types (Freestyle, Pipeline, Multibranch pipeline) for the Java Maven project with Jenkinsfile to:
>1. Connect to the application’s git repository
>2. Build jar
>3. Build Docker Image
>4. Push to private Dockerhub repository
## Description in details:
### Install  Build Tools (Maven, Node) in Jenkins
#### Option 1: Install tools using Jenkins Plugins
__Step 1:__ Install Plugin for Maven in UI `Manage Jenkins/Manage Plugins` if you don't have it

__Step 2:__ Open `Manage Jenkins/Global Tool Configuration` and add Maven plugin

_Note: You can do the same with Node_
#### Option 2: Install tools in Jenkins container
__Step 1:__ Enter docker container as `root` user (adding `-u 0`)
```sh
docker exec -u 0 -it "container_id" bash
```
__Step 2:__ Install `curl` if it's not available
```sh
apt install curl
```
__Note:__ Don't forget about `apt update`!

__Step 3:__ Install Node and npm ([Instalation guide](https://github.com/nodesource/distributions))
```sh
curl -fsSL https://deb.nodesource.com/setup_19.x | bash - 

apt-get install -y nodejs
```
__Step 4:__ Just check Node and npm
```sh
node -v
npm -v
```
### Make Docker available in Jenkins
__Step 1__: Create new Jenkins container with necessary volumes
```sh
docker run -p 8080:8080 -p 50000:50000 -d \
-v jenkins_home:/var/jenkins_home \
-v/var/run/docker.sock:/var/run/docker.sock \
--name jenkins jenkins/jenkins:lts-jdk11
```
__Step 2:__ log in as root inside `jenkins` container and install docker 
```sh
apt update
apt install docker.io
```
__Step 3:__ Return on machine and modify docker.sock permission
```sh
chmod 666 /var/run/docker.sock
```
### Create Jenkins credentials for a git repository
__Step 1:__ Go to `Manage Jenkins/Manage credenntials` and create git credentials 
* Kind: Username and password
* Scope: Global
* Username: username_of_git
* Password: pwd_of_git 
* ID: name of credentials(whatever you want)


### Create different Jenkins job types
##### _Note: Use `Jenkins-job` branch_
#### *Freestyle*  
__Step 1:__ Open Jenkins UI and create new job, chose `Freestyleproject` 
* configure `Build steps`
>__Exaple: 1.__ `Execute shell`
>```sh
>npm --verion
>``` 
>__Example 2:__ 
>configure in `Build steps` `invoke top-level Maven targets`
>*Maven version:
>*Goals: --version

#### *Pipeline*
__Before:__
* Install `Maven` inside Jenkins container
* Create Git credentials
* Create Pivate repo(Dockerhub in this case) credentials
* Condigure Private repo variables in `script.groovy` file and assign repo name!

__Step 1:__ Open Jenkins UI and create new job, chose `Pipeline`

__Step 2:__ switch on tab `pipeline` and choose  definition: `Pipeline script from SCM` and configure it:
1. SCM: Git 
2. Repository URL: YOUR_REPO_URL 
3. Credentials: credentials for git
4. Branch: branch that you want to use
5. Script: `Jenkinsfile` (or another file)


__Step 3:__ In Jenkins UI start `Build Now`

#### Multibranch pipeline
__Step 1:__ Open Jenkins UI and create new job, chose `Multibranch Pipeline`

__Step 2:__ configure it: choose what branches you want to add 

_Note: You can use the same Jenkins file in different branches_

__Step 3:__ You can configure this Jenkins file to execute some tasks __only on dedicated branch__ 
_Example: Add this block of code into Jenkins file_
```groovy
stage('build'){
    when {
        expression {
            BRANCH_NAME == 'main'
        }    
    }
    steps {
        script{
            echo "Building the appliaction" 
        }
    }
}
```

