def buildJar() {
    echo "building the application..."
    sh 'mvn package'
} 

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'dockerhub-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t nekodanke/my-repo:2.0 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push nekodanke/my-repo:2.0'
    }
} 

def deployApp() {
    echo 'deploying the application...'
} 

return this
